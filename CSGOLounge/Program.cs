﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net.Http;

namespace CSGOLounge
{
    class Program
    {
        static void Main(string[] args)
        {
            Parsing("http://csgolounge.com/match?m=1830");
            Console.ReadLine();
            
        }


        private static async void Parsing(string website)
        {

            try
            {
                HttpClient http=new HttpClient();
                var response = await http.GetByteArrayAsync(website);
                String source = Encoding.GetEncoding("UTF-8").GetString(response, 0, response.Length - 1);
                source = WebUtility.HtmlDecode(source);
                HtmlDocument resultDocument=new HtmlDocument();
                resultDocument.LoadHtml(source);

                List<HtmlNode> teamslist =
                    resultDocument.DocumentNode.Descendants()
                        .Where(
                            x =>
                                (x.Name == "div" && x.Attributes["class"] != null &&
                                 x.Attributes["class"].Value.Contains("box-shiny-alt")))
                        .ToList();
                var node = teamslist[0];

                string word = node.InnerText;
                
                
                
                
                word = Regex.Replace(word, @"[^\u0020-\u007E]", string.Empty);
                string newWord = word.Replace(" ", String.Empty);
                int start = newWord.IndexOf("CET") + "CET".Length;
                int end = newWord.LastIndexOf('%');
                string wynik = newWord.Substring(start, end-start);

                //pelen wynik meczu z oddsami i nazwami teamow
                string wynik1 = wynik.Replace("%", string.Empty);

                
                int format = newWord.IndexOf("Bestof");
                string matchFormat = newWord.Substring(format+"Bestof".Length);

                //gotowy format meczu
                string matchFormatResult = matchFormat.Substring(0, 1);

                //pelen wynik meczu z oddsami i nazwami teamow
                Console.WriteLine(matchFormatResult);
                
                Console.WriteLine(wynik1);
                string team1withodds = wynik1.Split(new string[] {"vs"}, StringSplitOptions.None)[0];
                string team2withodds = wynik1.Split(new string[] { "vs" }, StringSplitOptions.None)[1];

                //wychwytuje oddsy team1
                Match myMatch;
                MatchCollection matchesCollection = Regex.Matches(team1withodds, @"\d+");
                if (matchesCollection.Count > 1)
                {
                    myMatch = matchesCollection[1];
                }
                else
                {
                    myMatch = matchesCollection[0];
                }
                //wychwytuje oddsy team2
                Match myMatch1;
                MatchCollection matchesCollection1 = Regex.Matches(team2withodds, @"\d+");
                if (matchesCollection1.Count > 1)
                {
                    myMatch1 = matchesCollection1[1];
                }
                else
                {
                    myMatch1 = matchesCollection1[0];
                }


                int team1odds = Int32.Parse(Regex.Match(team1withodds, @"\d+").Value);
                int team2odds = Int32.Parse(Regex.Match(team2withodds, @"\d+").Value);
                Console.WriteLine("Team 1: " + team1withodds);
                Console.WriteLine("Team 2: " + team2withodds);
                Console.WriteLine("Team 1 odds: " + myMatch.Value);
                Console.WriteLine("Team 2 odds: " + myMatch1.Value);
                string team1name = team1withodds.Replace(myMatch.Value, "");
                string team2name = team2withodds.Replace(myMatch1.Value, "");
                Console.WriteLine("Team 1 name with win:" + team1name);//nazwa teamu1 z ewentualnym winem
                Console.WriteLine("Team 2 name with win:" + team2name);//nazwta teamu2 z ewentualnym winem

                string winningteam="Tie";
                string team1wowin;
                string team2wowin;
                //wywalenie wina z nazw
                if (team1name.Contains("(win)"))
                {
                    team1wowin = team1name.Replace("(win)", "");
                    winningteam = team1wowin;
                }

                else
                {
                    team1wowin = team1name;
                }

                if (team2name.Contains("(win)"))
                {
                    team2wowin = team2name.Replace("(win)", "");
                    winningteam = team2wowin;
                }

                else
                {
                    team2wowin = team2name;
                }

                Console.WriteLine("Team 1 name without win:" + team1wowin);//nazwa teamu1 bez wina
                Console.WriteLine("Team 2 name without win:" + team2wowin);//nazwta teamu2 bez wina
                Console.WriteLine("Winner was: "+ winningteam);




                //Console.WriteLine(newWord);










            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }

        }





    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSGOLoungeDataExtractor
{
    public class MatchCreator
    {
        private CSGOMatch CsgoMatch=null;
        public MatchCreator(string teamonename, string teamtwoname, string winningteamname, int teamoneodds,
            int teamtwoodds, string matchformat)
        {
            CsgoMatch=new CSGOMatch()
            {
                TeamOneName = teamonename,
                TeamTwoName = teamtwoname,
                WinningTeamName = winningteamname,
                TeamOneOdds = teamoneodds,
                TeamTwoOdds = teamtwoodds,
                MatchFormat = matchformat,
            };
        }

        public CSGOMatch GetCSGOMatch()
        {
            return CsgoMatch;
        }
    }
}

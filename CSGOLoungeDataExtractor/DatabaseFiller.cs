﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSGOLoungeDataExtractor
{
    public class DatabaseFiller
    {
        private MatchesContext Context = null;

        private CSGOMatch CurrentMatch = null;

        private DivPuller divPuller = null;

        private DataParser dataParser = null;

        private int MatchId;

        private string WebAddress = "http://csgolounge.com/match?m=";

        private int BeginFilling = 0;

        private int EndFilling = 0;



        public DatabaseFiller(int begin, int end)
        {
            
            EndFilling = end;

            Context=new MatchesContext();

            for (BeginFilling = begin; BeginFilling < EndFilling; BeginFilling++)
            {
                MatchId = BeginFilling;
                divPuller = new DivPuller(WebAddress + MatchId.ToString());

                dataParser = new DataParser(divPuller.GetResult());
                
                CurrentMatch=new CSGOMatch()
                {
                    TeamOneName = dataParser.GetTeamOneName(),
                    TeamTwoName = dataParser.GetTeamTwoName(),
                    MatchFormat = dataParser.GetMatchFormat(),
                    TeamOneOdds = dataParser.GetTeamOneOdds(),
                    TeamTwoOdds = dataParser.GetTeamTwoOdds(),
                    WinningTeamName = dataParser.GetWinningTeamName(),

                };

                Context.CsgoMatches.Add(CurrentMatch);
                Context.SaveChanges();

            }
            


        }
    }
}

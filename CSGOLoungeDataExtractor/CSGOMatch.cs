﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSGOLoungeDataExtractor
{
    public class CSGOMatch
    {
        public string TeamOneName { get; set; }
        public string TeamTwoName { get; set; }
        public string WinningTeamName { get; set; }
        public int TeamOneOdds { get; set; }
        public int TeamTwoOdds { get; set; }
        public string MatchFormat { get; set; }


    }
}

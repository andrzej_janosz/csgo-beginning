﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace CSGOLoungeDataExtractor
{
    public class DivPuller
    {
        private static string websiteAddress=null;
        private static string result = null;

        public DivPuller(string website)
        {
            websiteAddress = website;
            
        }


        private static async void ExtractStringFromDiv()
        {
            HttpClient http = new HttpClient();
            var response = await http.GetByteArrayAsync(websiteAddress);
            String source = Encoding.GetEncoding("UTF-8").GetString(response, 0, response.Length - 1);
            source = WebUtility.HtmlDecode(source);
            HtmlDocument resultDocument = new HtmlDocument();
            resultDocument.LoadHtml(source);

            List<HtmlNode> teamslist =
                resultDocument.DocumentNode.Descendants()
                    .Where(
                        x =>
                            (x.Name == "div" && x.Attributes["class"] != null &&
                             x.Attributes["class"].Value.Contains("box-shiny-alt")))
                    .ToList();
            var node = teamslist[0];

            result = node.InnerText;
        }

        public string GetResult()
        {
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CSGOLoungeDataExtractor
{
    public class MatchesContext : DbContext
    {

        public MatchesContext() : base("CSGOMatchesDatabase") 
        {
            Database.SetInitializer<MatchesContext>(new CreateDatabaseIfNotExists<MatchesContext>());
        }
        public DbSet<CSGOMatch> CsgoMatches { get; set; }
    }
}
